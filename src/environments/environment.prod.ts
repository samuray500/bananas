export const environment = {
  production: true,
  apiUrl: 'https://staging-frontapi.cherrytech.com/',
  apiBrand: 'cherrycasino.desktop',
  apiLocale: 'en',
  themeColor: '#eed133',
  themeBlueColor: '#6698bd'
};
